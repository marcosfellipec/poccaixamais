//
//  FeaturesConst.swift
//  POCCAIXA+
//
//  Created by gemed on 06/05/2019.
//  Copyright © 2019 gemed. All rights reserved.
//


import UIKit
import Transferencia
import CaixaBase


enum StoryboardNames: String {
    case transferencia = "Transferencia"
}


class FeaturesConst: NSObject {


    public static func get(_ feature: StoryboardNames) -> UIViewController? {
        var storyboard: UIStoryboard

        switch feature {
        case .transferencia:
            storyboard = UIStoryboard(name: feature.rawValue, bundle: Bundle(for: TransferenciaViewController.self))

        default:
            return nil
        }

        guard let vc = storyboard.instantiateInitialViewController() else {
            return nil
        }
        return vc
    }
}

public enum CardFeatures: String, CaseIterable {
    case pagamento = "CaixaPagamento.PagamentoCardView"
    case transferencia = "CaixaTransferencia.TransferenciaCardView"
}

public class CardFeaturesLib {
    public static func getCardInstance(cardName: CardFeatures) -> CardBaseView? {
        guard let classInst = NSClassFromString(cardName.rawValue) as? NSObject.Type else {
            return nil
        }
        if let cardInstance = classInst.init() as? CardBaseView {
            return cardInstance
        }

        return nil
    }
}
