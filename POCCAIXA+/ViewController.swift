//
//  ViewController.swift
//  MeuOvo
//
//  Created by gemed on 03/05/2019.
//  Copyright © 2019 gemed. All rights reserved.
//

import UIKit
import CaixaBase
import CaixaPagamento
import CaixaTransferencia
//import Navigator

class ViewController: UIViewController {

    @IBOutlet weak var buttonTransferencia: UIButton!
    
    @IBAction func actionButton(_ sender: Any) {
        
        guard let vc = FeaturesConst.get(.transferencia) else {
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var arrCards:[CardBaseView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    buttonTransferencia.layer.cornerRadius = 20
        // Do any additional setup after loading the view, typically from a nib.
//        setupCards()
    }
    
    private func setupCards() {
        for cardName in CardFeatures.allCases {
            if let cardInstance = CardFeaturesLib.getCardInstance(cardName: cardName) {
                arrCards.append(cardInstance)
            }
        }

        for (index, cardItem) in arrCards.enumerated() {
            let bottomConstraint: NSLayoutYAxisAnchor
            if (index == 0) {
                bottomConstraint = view.safeAreaLayoutGuide.bottomAnchor
            } else {
                bottomConstraint = arrCards[index-1].topAnchor
            }

            view.addSubview(cardItem)
            cardItem.translatesAutoresizingMaskIntoConstraints = false
            cardItem.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
            cardItem.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
            cardItem.bottomAnchor.constraint(equalTo: bottomConstraint, constant: -10).isActive = true
            cardItem.heightAnchor.constraint(equalToConstant: 200).isActive = true
            cardItem.delegate = self
        }
    }
}

extension ViewController: CardDelegate {
    func didOpenCard(cardView: CardBaseView) {
        for card in arrCards {
            if card !== cardView {
                card.closeCard()
            }
        }
    }

    func didClosedCard(cardView: CardBaseView) {
        let cardName = NSStringFromClass(type(of: cardView))
        print("Click Card Base - Closed \(cardName)")
    }
}
