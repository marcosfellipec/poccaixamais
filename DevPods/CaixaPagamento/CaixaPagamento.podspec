Pod::Spec.new do |spec|
  spec.name         = "CaixaPagamento"
  spec.version      = "1.0.0"
  spec.summary      = "Componentes reutilizaveis da Caixa"
  spec.description  = <<-DESC
			Componentes reutilizaveis da Caixa
                       DESC

  spec.license      = { :type => 'MIT', :file => 'LICENSE' }
  spec.source       = { :git => "https://gitlab.com/foton-caixasp/PODS/CaixaPagamento.git", :tag => "#{spec.version}" }
  spec.homepage     = "www.foton.la"
  spec.author       = { "Foton SA." => "contato@foton.la" }
  spec.platform     = :ios, "10.0"
  
  spec.dependency 'CaixaBase'

  spec.subspec 'CaixaBase' do |ss|
    ss.source_files = '../DevPods/CaixaBase/**/*.swift'
    ss.resource = '../DevPods/CaixaBase/**/*.xib'
  end


  spec.source_files = 'Pod/**/*.swift'
  spec.resources    = 'Pod/**/*.{xib,xcassets}'
end
