//
//  PagamentoCardView.swift
//  CaixaBase
//
//  Created by foton on 09/05/19.
//

import UIKit
import CaixaBase

public class PagamentoCardView: CardBaseView {
    
    let customLabel = UILabel()
    let customAction = UIButton()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        setup()
    }
    
    private func setup() {
        backgroundColor = UIColor.orange
        addSubview(customLabel)
        addSubview(customAction)
        customLabel.translatesAutoresizingMaskIntoConstraints = false
        customAction.translatesAutoresizingMaskIntoConstraints = false
        customLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        customLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
        customLabel.bottomAnchor.constraint(equalTo: customAction.topAnchor, constant: -10).isActive = true
        customAction.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        customAction.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
        customAction.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        customLabel.text = "Card de Pagamentos"
        customLabel.backgroundColor = UIColor.purple
        customLabel.textAlignment = .center
        customAction.setTitle("Click Card Pagamentos", for: UIControl.State.normal)
        customAction.backgroundColor = UIColor.gray
        customAction.addTarget(self, action: #selector(clickButtonPagamentos), for: UIControl.Event.touchUpInside)
    }
    
    @objc private func clickButtonPagamentos() {
        print("Click Card Pagamentos")
    }
    
}
