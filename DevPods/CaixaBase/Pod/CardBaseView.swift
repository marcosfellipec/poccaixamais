//
//  CardBaseViewController.swift
//  Pods-POCCAIXA+
//
//  Created by foton on 09/05/19.
//

import UIKit

public protocol CardDelegate {
    func didOpenCard(cardView: CardBaseView)
    func didClosedCard(cardView: CardBaseView)
}

public enum CardState {
    case open
    case closed
}

open class CardBaseView: UIView {
    
    public let statusCard = UILabel()
    public let buttonAction = UIButton()
    
    public var delegate: CardDelegate?
    
    private(set) public var cardState: CardState = .closed
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        setup()
    }
    
    private func setup() {
        backgroundColor = UIColor.yellow
        addSubview(statusCard)
        addSubview(buttonAction)
        statusCard.translatesAutoresizingMaskIntoConstraints = false
        buttonAction.translatesAutoresizingMaskIntoConstraints = false
        statusCard.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        statusCard.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        statusCard.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
        buttonAction.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        buttonAction.topAnchor.constraint(equalTo: statusCard.bottomAnchor, constant: 10).isActive = true
        buttonAction.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
        statusCard.text = "Initial Card State - Closed"
        statusCard.backgroundColor = UIColor.green
        statusCard.textAlignment = .center
        buttonAction.setTitle("Click Card Base - Open", for: UIControl.State.normal)
        buttonAction.backgroundColor = UIColor.red
        buttonAction.addTarget(self, action: #selector(clickButton), for: UIControl.Event.touchUpInside)
    }
    
    @objc private func clickButton() {
        if (cardState == .open) {
            closeCard()
            delegate?.didClosedCard(cardView: self)
        } else if (cardState == .closed) {
            openCard()
            delegate?.didOpenCard(cardView: self)
        }
    }
    
    public func openCard() {
        statusCard.text = "Card Open"
        buttonAction.setTitle("Click Card Base - Close", for: UIControl.State.normal)
        cardState = .open
    }
    
    public func closeCard() {
        statusCard.text = "Card Closed"
        buttonAction.setTitle("Click Card Base - Open", for: UIControl.State.normal)
        cardState = .closed
    }
    
}
