//
//  TransferenciaCardView.swift
//  CaixaBase
//
//  Created by foton on 09/05/19.
//

import UIKit
import CaixaBase

public class TransferenciaCardView: CardBaseView {
    
    let customLabel = UILabel()
    let customAction = UIButton()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        setup()
    }
    
    private func setup() {
        backgroundColor = UIColor.cyan
        addSubview(customLabel)
        addSubview(customAction)
        customLabel.translatesAutoresizingMaskIntoConstraints = false
        customAction.translatesAutoresizingMaskIntoConstraints = false
        customLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        customLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
        customLabel.bottomAnchor.constraint(equalTo: customAction.topAnchor, constant: -10).isActive = true
        customAction.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        customAction.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
        customAction.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        customLabel.text = "Card de Transferência"
        customLabel.backgroundColor = UIColor.magenta
        customLabel.textAlignment = .center
        customAction.setTitle("Click Card Transferência", for: UIControl.State.normal)
        customAction.backgroundColor = UIColor.blue
        customAction.addTarget(self, action: #selector(clickButtonTransferencia), for: UIControl.Event.touchUpInside)
    }
    
    @objc private func clickButtonTransferencia() {
        print("Click Card Transferencia")
    }
    
}
